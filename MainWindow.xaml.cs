﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AES_encryption_files
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonEncrypt_Click(object sender, RoutedEventArgs e)
        {
            int counter = 0;
            var tasks = new List<Task>();
            var path = textBox_folderToEncrypt.Text;
            var password = textBox_passwordToEncrypt.Text;
            ZipDirectories(path);
            var allFiles = GetFiles(path);
            int totalSize = allFiles.Length;
            foreach (var file in allFiles)
            {
                tasks.Add(
                Task.Factory.StartNew(() =>
                {
                    Cryptography.EncryptFile(file, password);
                    using (var md5 = MD5.Create())
                    {
                        using (var stream = File.OpenRead(file))
                        {
                            listBoxEncryptedHash.Dispatcher.Invoke(() => listBoxEncryptedHash.Items.Add(BitConverter.ToString(md5.ComputeHash(stream)) + "?" + file));
                        }
                    }
                    counter++;
                    ProgressBarEncrypt.Dispatcher.Invoke(() => ProgressBarEncrypt.Value = (counter / totalSize) * 100);
                })); 
            }
            
    }

        private void buttonDecrypt_Click(object sender, RoutedEventArgs e)
        {
            int counter = 0;
            var tasks = new List<Task>();
            var path = textBox_folderToDecrypt.Text;
            var password = textBox_passwordToDecrypt.Text;
            var allFiles = GetFiles(path);
            int totalSize = allFiles.Length;
            foreach (var file in allFiles)
            {
                tasks.Add(
                Task.Factory.StartNew(() =>
                {
                    string currentMD5;
                    using (var md5 = MD5.Create())
                    {
                        using (var stream = File.OpenRead(file))
                        {
                            currentMD5 = BitConverter.ToString(md5.ComputeHash(stream));       
                        }
                    }
                    var listBoxItems = listBoxEncryptedHash.Items;
                    foreach(string item in listBoxItems)
                    {
                        int i = 1;
                        string[] parts = item.Split('?');
                        if(parts[i] == file)
                        {
                            if(parts[i-1] == currentMD5)
                            {
                                Cryptography.DecryptFile(file, password);
                               listBoxHashForDecryption.Dispatcher.Invoke(() => listBoxHashForDecryption.Items.Add(file + " MD5 OK, Decrypting"));
                            } else
                            {
                                listBoxHashForDecryption.Dispatcher.Invoke(() => listBoxHashForDecryption.Items.Add(file + " MD5 Failed, not decrypting"));
                            }
                        }
                    }
                    counter++;
                    ProgressBarDecrypt.Dispatcher.Invoke(() => ProgressBarDecrypt.Value = (counter / totalSize) * 100);
                }));
            }
            Task.WaitAll();
        }

        private string[] GetFiles(string path)
        {
            var files = Directory.GetFiles(path);
            return files;
        }
        private void ZipDirectories(string path)
        {
            var directories = Directory.GetDirectories(path);
            foreach(var directory in directories)
            {
                ZipFile.CreateFromDirectory(directory, directory + ".zip");
            }
        }
    }
}
